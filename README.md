# Introduction aux réseaux de neurones

Le matériel de cette séance pratique est adapté du cours "Introduction aux réseaux de neurones" enseigné en Master 2 à l'Université de Lille.

Site web du cours: http://chercheurs.lille.inria.fr/pgermain/neurones2018/index.html  
GitHub sur l'introduction à python: https://github.com/pgermain/cours2018-Intro_a_Python  
Github sur les réseaux de neurones: https://github.com/pgermain/cours2018-Intro_aux_reseaux_de_neurones  

### Comment télécharger le contenu de ce répertoire Git.

Pour télécharger le contenu de ce répertoire Git, il suffit d'exécuter la commande suivante dans un terminal:
```
git clone https://gitlab.inria.fr/pgermain/tdmodal-reseaux_de_neurones.git
```
Cela créera un sous-répertoire `tdmodal-reseaux_de_neurones` dans le répertoire courant de votre ordinateur.
Tant que vous ne modifiez pas localement les fichiers dans ce répertoire (vous pouvez les copier à un autre endroit puis les modifier à votre guise), vous pouvez mettre à jour le contenu comme suit:
1. Accéder au répertoire local:
   ```
   cd tdmodal-reseaux_de_neurones
   ```
2. Télécharger la nouvelle version à partir de GitLab: 
   ```
   git pull
   ```

# Installation de Python

Je conseille à tous d'installer la distribution Python [Anaconda](https://www.anaconda.com/).

* Télécharger la version d'Anaconda avec Python 3.7 pour votre système d'exploitation (Linux, MacOS ou Windows):  
https://www.anaconda.com/download/

* Installer Anaconda en suivant les instructions:
https://docs.anaconda.com/anaconda/install/#detailed-installation-information

* Vous devrez possiblement rédémarrer la session de votre utilisateur pour initialiser les variables d'environnement.

* Au besoin, mettre à jour Anaconda. Par ligne de commande:  
```
conda update --all
```

## Installation de la librairie *pyTorch*

Nous utiliserons la librairie [pyTorch](https://pytorch.org/) pour s'initier aux réseaux de neurones.
Pour l'installer à partir d'*Anaconda*, il suffit d'exécuter la commande suivante dans un terminal:

* Sous Linux:  
 ```
conda install pytorch-cpu torchvision-cpu -c pytorch
```

* Sous MacOS:  
 ```
conda install pytorch torchvision -c pytorch
```

* Sous Windows:  
```
conda install pytorch-cpu torchvision-cpu -c pytorch
```

## Jupyter notebook



Dans le cadre du TD, nous allons développer dans un *carnet [Jupyter](http://jupyter.org/)*.
Démarrer Jupyter pour commencer:  
```
jupyter notebook
```


# Pour aller plus loin
Voici d'autres renseignements non-nécessaires pour le TD, tirés du matériel que je donne aux étudiants de Master qui suivent mon cours. **Vous pouvez donc arrêter votre lecture ici!**


### Mode de développement interactif

* En plus du *Jupyter notebook*, il peut parfois être pratique d'exécuter IPython dans un terminal pour faire quelques essais rapide:   
```
ipython
```

#### Mode de développement script
Une manière plus conventionelle de programmer en python est d'écrire son code dans un (ou plusieurs) fichier(s) texte(s) et de l'exécuter ensuite à l'aide de l'interpréteur python. Il existe plusieurs environnement de développement pour vous assister dans cette tâche. Par exemple:

* Visual Studio Code: https://code.visualstudio.com/
* PyCharm https://www.jetbrains.com/pycharm/

#### Quelques tutoriels suggérés sur Python

* Python et data science: http://www.scipy-lectures.org/
* Scikit-Learn: http://scikit-learn.org/stable/tutorial/basic/tutorial.html
* Tutoriel officiel de Python (avec beaucoup de détail): https://docs.python.org/3/tutorial/index.html
* Un bon survol de concepts simples et avancés: https://github.com/jakevdp/WhirlwindTourOfPython
